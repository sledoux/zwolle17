import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { MatButtonModule } from '@angular/material/button';

import { AppComponent } from './app.component';
import { GrilleComponent } from './grille/grille.component';
import { GrilleColonnesComponent } from './grille-colonnes/grille-colonnes.component';

@NgModule({
  declarations: [
    AppComponent,
    GrilleComponent,
    GrilleColonnesComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    DragDropModule,
    MatButtonModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
