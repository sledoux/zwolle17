import { Component, OnInit } from '@angular/core';
import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';

@Component({
  selector: 'app-grille',
  templateUrl: './grille.component.html',
  styleUrls: ['./grille.component.scss']
})
export class GrilleComponent implements OnInit {

  nbLines = 27;
  lines: number[] = Array(this.nbLines).fill(0).map((x, i) => i);
  started = false;
  interval;

  constructor() {
  }

  ngOnInit() {
  }

  drop(event: CdkDragDrop<string[]>) {
    moveItemInArray(this.lines, event.previousIndex, event.currentIndex);
  }

  toggleShuffle() {
    this.started = !this.started;
    clearInterval(this.interval);
    if (this.started) {
      this.interval = setInterval(() => {
        this.shuffle();
      }, 1000);
    }
  }

  shuffle() {
    const len = this.lines.length;
    let i = len;
    while (i--) {
      const p = Math.floor(Math.random() * len);
      const t = this.lines[i];
      this.lines[i] = this.lines[p];
      this.lines[p] = t;
    }
    this.lines = [...this.lines];
  }

}
