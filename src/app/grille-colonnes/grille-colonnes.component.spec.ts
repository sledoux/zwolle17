import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GrilleColonnesComponent } from './grille-colonnes.component';

describe('GrilleColonnesComponent', () => {
  let component: GrilleColonnesComponent;
  let fixture: ComponentFixture<GrilleColonnesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GrilleColonnesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GrilleColonnesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
