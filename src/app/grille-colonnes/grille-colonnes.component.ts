import { Component, OnInit } from '@angular/core';
import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';

@Component({
  selector: 'app-grille-colonnes',
  templateUrl: './grille-colonnes.component.html',
  styleUrls: ['./grille-colonnes.component.scss']
})
export class GrilleColonnesComponent implements OnInit {


  nbCols = 24;
  cols: number[] = Array(this.nbCols).fill(0).map((x, i) => i);
  started = false;
  interval;

  constructor() {
  }

  ngOnInit() {
  }

  drop(event: CdkDragDrop<string[]>) {
    moveItemInArray(this.cols, event.previousIndex, event.currentIndex);
  }

  toggleShuffle() {
    this.started = !this.started;
    clearInterval(this.interval);
    if (this.started) {
      this.interval = setInterval(() => {
        this.shuffle();
      }, 1000);
    }
  }

  shuffle() {
    const len = this.cols.length;
    let i = len;
    while (i--) {
      const p = Math.floor(Math.random() * len);
      const t = this.cols[i];
      this.cols[i] = this.cols[p];
      this.cols[p] = t;
    }
    this.cols = [...this.cols];
  }
}
