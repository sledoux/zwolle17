# Zwolle17

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 8.3.18.

## Pour lancer l'application

### 0) Prérequis

Il faut installer nodejs sur votre poste : https://nodejs.org/en/

Prenez la version LTS (actuellement 12.13.0). Cela installe node et npm.

### 1) Clonez le repository

Récupérez les sources (par git clone, ou téléchargement du zip).

### 2) Installez les dépendances

Ouvrez une invite de commande dans le dossier que vous venez de récupérer.

Exécutez la commande `npm install` pour télécharger les dépendances du projet. Cela peut prendre quelques minutes.

### 3) Lancez l'application

Ensuite, exécutez la commande `npm start` pour build l'application de l'exposer sur le port 4200.

Ouvrez votre navigateur sur la page `http://localhost:4200/` pour accéder à l'application.
